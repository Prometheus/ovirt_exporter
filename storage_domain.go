package main

import (
	"fmt"
	"io"
)

type StorageDomainResponse struct {
	StorageDomains []StorageDomain `json:"storage_domain"`
}

type StorageDomain struct {
	ID        string `json:"id"`
	Name      string `json:"name"`
	Available string `json:"available"`
	Committed string `json:"committed"`
	Used      string `json:"used"`
}

func (d *DataCenter) StorageDomains() ([]StorageDomain, error) {
	resp := StorageDomainResponse{}

	err := d.ovirt.get(d.Path+"/storagedomains", &resp)
	if err != nil {
		return nil, err
	}

	return resp.StorageDomains, nil
}

func (s *StorageDomain) WriteMetrics(w io.Writer) {
	fmt.Fprintf(w, "ovirt_storage_domain{storage_domain=%q,storage_domain_name=%q} 1\n", s.ID, s.Name)
	fmt.Fprintf(w, "ovirt_storage_domain_available_bytes{storage_domain=%q} %s\n", s.ID, s.Available)
	fmt.Fprintf(w, "ovirt_storage_domain_committed_bytes{storage_domain=%q} %s\n", s.ID, s.Committed)
	fmt.Fprintf(w, "ovirt_storage_domain_used_bytes{storage_domain=%q} %s\n", s.ID, s.Used)
}
