package main

import (
	"fmt"
	"io"
	"math"
	"strconv"
)

type QuotaResponse struct {
	Quotas []Quota `json:"quota"`
}

type Quota struct {
	ovirt *OVirt
	ID    string `json:"id"`
	Name  string `json:"name"`
	Path  string `json:"href"`
}

func (d *DataCenter) Quotas() ([]Quota, error) {
	resp := QuotaResponse{}

	err := d.ovirt.get(d.Path+"/quotas", &resp)
	if err != nil {
		return nil, err
	}

	for i := range resp.Quotas {
		resp.Quotas[i].ovirt = d.ovirt
	}

	return resp.Quotas, nil
}

func (q *Quota) WriteMetrics(w io.Writer) error {
	fmt.Fprintf(w, "ovirt_quota{quota=%q,quota_name=%q} 1\n", q.ID, q.Name)

	clusterLimits, err := q.ClusterLimits()
	if err != nil {
		return err
	}

	for _, clusterLimit := range clusterLimits {
		clusterLimit.WriteMetrics(w, q.ID)
	}

	storageLimits, err := q.StorageLimits()
	if err != nil {
		return err
	}

	for _, storageLimit := range storageLimits {
		storageLimit.WriteMetrics(w, q.ID)
	}

	return nil
}

type QuotaClusterLimitResponse struct {
	QuotaClusterLimits []QuotaClusterLimit `json:"quota_cluster_limit"`
}

type QuotaClusterLimit struct {
	MemoryUsage float64 `json:"memory_usage"`
	MemoryLimit float64 `json:"memory_limit"`
	VCPUUsage   string  `json:"vcpu_usage"`
	VCPULimit   string  `json:"vcpu_limit"`
}

func (q *Quota) ClusterLimits() ([]QuotaClusterLimit, error) {
	resp := QuotaClusterLimitResponse{}

	err := q.ovirt.get(q.Path+"/quotaclusterlimits", &resp)
	if err != nil {
		return nil, err
	}

	return resp.QuotaClusterLimits, nil
}

func (cl *QuotaClusterLimit) WriteMetrics(w io.Writer, quotaID string) {
	memoryLimit := cl.MemoryLimit * (1 << 30)
	if cl.MemoryLimit < 0 {
		memoryLimit = math.NaN()
	}

	vcpuLimit := cl.VCPULimit
	if vcpuLimit == "-1" {
		vcpuLimit = "NaN"
	}

	fmt.Fprintf(w, "ovirt_quota_memory_usage_bytes{quota=%q} %f\n", quotaID, cl.MemoryUsage*(1<<30))
	fmt.Fprintf(w, "ovirt_quota_memory_limit_bytes{quota=%q} %f\n", quotaID, memoryLimit)
	fmt.Fprintf(w, "ovirt_quota_vcpu_usage{quota=%q} %s\n", quotaID, cl.VCPUUsage)
	fmt.Fprintf(w, "ovirt_quota_vcpu_limit{quota=%q} %s\n", quotaID, vcpuLimit)
}

type QuotaStorageLimitResponse struct {
	QuotaStorageLimits []QuotaStorageLimit `json:"quota_storage_limit"`
}

type QuotaStorageLimit struct {
	Usage         float64 `json:"usage"`
	Limit         string  `json:"limit"`
	StorageDomain struct {
		ID   string `json:"id"`
		Path string `json:"href"`
	} `json:"storage_domain"`
}

func (q *Quota) StorageLimits() ([]QuotaStorageLimit, error) {
	resp := QuotaStorageLimitResponse{}

	err := q.ovirt.get(q.Path+"/quotastoragelimits", &resp)
	if err != nil {
		return nil, err
	}

	return resp.QuotaStorageLimits, nil
}

func (s *QuotaStorageLimit) WriteMetrics(w io.Writer, quotaID string) {
	limit, _ := strconv.ParseFloat(s.Limit, 64)
	limit *= 1 << 30
	if limit < 0 {
		limit = math.NaN()
	}

	fmt.Fprintf(w, "ovirt_quota_storage_usage_bytes{quota=%q,storage_domain=%q} %f\n", quotaID, s.StorageDomain.ID, s.Usage*(1<<30))
	fmt.Fprintf(w, "ovirt_quota_storage_limit_bytes{quota=%q,storage_domain=%q} %f\n", quotaID, s.StorageDomain.ID, limit)
}
