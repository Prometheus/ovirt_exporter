package main

import (
	"fmt"
	"io"
)

type VMsResponse struct {
	VMs []VM `json:"vm"`
}

type VM struct {
	ovirt *OVirt

	Path     string `json:"href"`
	Name     string `json:"name"`
	Status   string `json:"status"`
	StopTime int64  `json:"stop_time"`
}

func (o *OVirt) VMs() ([]VM, error) {
	resp := &VMsResponse{}

	err := o.get("ovirt-engine/api/vms", &resp)
	if err != nil {
		return nil, err
	}

	for i := range resp.VMs {
		resp.VMs[i].ovirt = o
	}

	return resp.VMs, nil
}

func (vm *VM) WriteMetrics(w io.Writer) error {
	status := 1

	if vm.Status != "up" {
		status = 0
	}

	fmt.Fprintf(w, "ovirt_vm_status{name=%q,status=%q} %d\n", vm.Name, vm.Status, status)

	snapshots, err := vm.Snapshots()
	if err != nil {
		return err
	}

	for _, s := range snapshots {
		if s.SnapshotType == "active" {
			continue
		}

		s.WriteMetrics(w)
	}

	return nil
}
