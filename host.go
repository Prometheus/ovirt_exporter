package main

import (
	"fmt"
	"io"
)

type HostsResponse struct {
	Hosts []Host `json:"host"`
}

type Host struct {
	ovirt *OVirt

	Name            string `json:"name"`
	Status          string `json:"status"`
	UpdateAvailable string `json:"update_available"`
}

func (o *OVirt) Hosts() ([]Host, error) {
	resp := &HostsResponse{}

	err := o.get("ovirt-engine/api/hosts", &resp)
	if err != nil {
		return nil, err
	}

	for i := range resp.Hosts {
		resp.Hosts[i].ovirt = o
	}

	return resp.Hosts, nil
}

func (h *Host) WriteMetrics(w io.Writer) {
	status := 1
	updateAvailable := 0

	if h.Status != "up" {
		status = 0
	}

	if h.UpdateAvailable == "true" {
		updateAvailable = 1
	}

	fmt.Fprintf(w, "ovirt_host_status{status=%q} %d\n", h.Name, status)
	fmt.Fprintf(w, "ovirt_host_status{status=%q} %d\n", h.Name, updateAvailable)
}
