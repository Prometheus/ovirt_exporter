package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"

	"gitlab.com/silkeh/env"
)

type OVirt struct {
	url, user, pass string
	client          http.Client
	bearer          string
}

func NewOVirt(url, user, pass string) *OVirt {
	return &OVirt{
		url:  url,
		user: user,
		pass: pass,
		client: http.Client{
			Transport:     nil,
			CheckRedirect: nil,
			Timeout:       0,
		},
	}
}

func (o *OVirt) request(req *http.Request, v interface{}, reauth bool) error {
	resp, err := o.client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		if reauth && resp.StatusCode == http.StatusUnauthorized {
			if err := o.Login(); err != nil {
				return fmt.Errorf("error logging in after 401: %w", err)
			}

			return o.request(req, v, false)
		}

		return fmt.Errorf("unexpected status code: %v", resp.StatusCode)
	}

	return json.NewDecoder(resp.Body).Decode(v)
}

func (o *OVirt) get(path string, v interface{}) error {
	req, _ := http.NewRequest(http.MethodGet, o.url+"/"+path, nil)
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Authorization", "Bearer "+o.bearer)

	return o.request(req, v, true)
}

func writeMetrics(w io.Writer, o *OVirt) error {
	datacenters, err := o.DataCenters()
	if err != nil {
		return err
	}

	for _, datacenter := range datacenters {
		err = datacenter.WriteMetrics(w)
		if err != nil {
			return err
		}
	}

	hosts, err := o.Hosts()
	if err != nil {
		return err
	}

	for _, host := range hosts {
		host.WriteMetrics(w)
	}

	vms, err := o.VMs()
	if err != nil {
		return err
	}

	for _, vm := range vms {
		err = vm.WriteMetrics(w)
		if err != nil {
			return err
		}
	}

	return nil
}

func main() {
	var url, user, pass, addr string

	flag.StringVar(&url, "ovirt-url", "http://localhost", "oVirt URL")
	flag.StringVar(&user, "ovirt-username", "admin@internal", "Username")
	flag.StringVar(&pass, "ovirt-password", "", "Password")
	flag.StringVar(&addr, "listen-address", ":9199", "Listen address")

	err := env.ParseWithFlags()
	if err != nil {
		log.Fatalf("Error parsing flags: %s", err)
	}

	ovirt := NewOVirt(url, user, pass)
	if err = ovirt.Login(); err != nil {
		log.Fatalf("Unable to connect to oVirt: %s", err)
	}

	ovirt.Hosts()

	http.HandleFunc("/metrics", func(writer http.ResponseWriter, request *http.Request) {
		if err := writeMetrics(writer, ovirt); err != nil {
			log.Printf("Error writing metrics: %s", err)

			writer.WriteHeader(http.StatusInternalServerError)
		}
	})

	log.Printf("Listening on %q", addr)
	log.Fatal(http.ListenAndServe(addr, nil))
}
