package main

import (
	"fmt"
	"io"
)

type DataCenterResponse struct {
	DataCenters []DataCenter `json:"data_center"`
}

type DataCenter struct {
	ovirt *OVirt
	ID    string `json:"id"`
	Name  string `json:"name"`
	Path  string `json:"href"`
}

func (o *OVirt) DataCenters() ([]DataCenter, error) {
	resp := DataCenterResponse{}

	err := o.get("ovirt-engine/api/datacenters", &resp)
	if err != nil {
		return nil, err
	}

	for i := range resp.DataCenters {
		resp.DataCenters[i].ovirt = o
	}

	return resp.DataCenters, nil
}

func (d *DataCenter) WriteMetrics(w io.Writer) error {
	fmt.Fprintf(w, "ovirt_datacenter{datacenter=%q,datacenter_name=%q} 1\n", d.ID, d.Name)

	storageDomains, err := d.StorageDomains()
	if err != nil {
		return err
	}

	for _, s := range storageDomains {
		s.WriteMetrics(w)
	}

	quotas, err := d.Quotas()
	if err != nil {
		return err
	}

	for _, quota := range quotas {
		if err := quota.WriteMetrics(w); err != nil {
			return err
		}
	}
	return nil
}
