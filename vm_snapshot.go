package main

import (
	"fmt"
	"io"
)

type SnapshotsResponse struct {
	Snapshots []Snapshot `json:"snapshot"`
}

type Snapshot struct {
	ID           string `json:"id"`
	VM           VM     `json:"vm"`
	Description  string `json:"description"`
	Date         int    `json:"date"`
	SnapshotType string `json:"snapshot_type"`
}

func (vm *VM) Snapshots() ([]Snapshot, error) {
	resp := &SnapshotsResponse{}

	err := vm.ovirt.get(vm.Path+"/snapshots", &resp)
	if err != nil {
		return nil, err
	}

	return resp.Snapshots, nil
}

func (s *Snapshot) WriteMetrics(w io.Writer) {
	fmt.Fprintf(w, "ovirt_vm_snapshot_info{vm=%q,description=%q,type=%q,id=%q} %d\n",
		s.VM.Name, s.Description, s.SnapshotType, s.ID, 1)
	fmt.Fprintf(w, "ovirt_vm_snapshot_date{id=%q} %d\n", s.ID, s.Date)
}
