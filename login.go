package main

import (
	"net/http"
	"net/url"
	"strings"
)

type LoginResponse struct {
	AccessToken string `json:"access_token"`
	Scope       string `json:"scope"`
	Exp         string `json:"exp"`
	TokenType   string `json:"token_type"`
}

func (o *OVirt) Login() error {
	form := url.Values{}
	form.Set("grant_type", "password")
	form.Set("scope", "ovirt-app-api")
	form.Set("username", o.user)
	form.Set("password", o.pass)

	req, _ := http.NewRequest(http.MethodPost, o.url+"/ovirt-engine/sso/oauth/token", strings.NewReader(form.Encode()))
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	resp := LoginResponse{}

	if err := o.request(req, &resp, false); err != nil {
		return err
	}

	o.bearer = resp.AccessToken

	return nil
}
